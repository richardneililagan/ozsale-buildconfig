﻿namespace MySaleBaseProject.Classes.Data.Parser
{
    public enum eDateToStringConversion
    {
        eDateToStringLongDate,
        eDateToStringShortDate,
        eDateToStringLongTime,
        eDateToStringShortTime
    }

    public enum eTargetApp
    {
        OzSale,
        MySale,
        SingSale,
        NzSale,
        Cocosa,
        BuyInvite,
        LondonChic
    }
}